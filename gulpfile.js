var gulp = require('gulp'),
  minifyHTML = require('gulp-minify-html'),
  connect = require('gulp-connect'),
  sass = require('gulp-sass'),
  uglify = require('gulp-uglify'),
  browserify = require('browserify'),
  source = require('vinyl-source-stream'),
  transform = require('vinyl-transform'),
  sourcemaps = require('gulp-sourcemaps'),
  imagemin = require('gulp-imagemin'),
  pngquant = require('imagemin-pngquant');

gulp.task('connect', function () {
  connect.server({
    root: 'dist',
    livereload: true
  });
});

gulp.task('images', function () {
  return gulp.src('./src/images/*')
    .pipe(imagemin({
      progressive: true,
      svgoPlugins: [{removeViewBox: false}],
      use: [pngquant()]
    }))
    .pipe(gulp.dest('./dist/images'));
});

gulp.task('js', function () {
  var browserified = transform(function (filename) {
    var b = browserify(filename);
    return b.bundle();
  });

  gulp.src('./src/js/main.js')
    .pipe(sourcemaps.init({loadMaps: true}))
    .pipe(browserified)
    // Add transformation tasks to the pipeline here.
    .pipe(uglify())
    .pipe(sourcemaps.write('./'))
    .pipe(connect.reload())
    .pipe(gulp.dest('./dist/js/'));
});

gulp.task('minify-html', function () {
  var opts = {
    conditionals: true,
    spare: true
  };

  return gulp.src('./src/documents/*.html')
    .pipe(minifyHTML(opts))
    .pipe(gulp.dest('./dist/'))
    .pipe(connect.reload());
});

gulp.task('sass', function () {
  gulp.src('./src/scss/*.scss')
    .pipe(sass())
    .pipe(gulp.dest('./dist/css'))
    .pipe(connect.reload());
});

gulp.task('default', function () {
  gulp.start('connect');
  gulp.watch('src/**/*.scss', ['sass']);
  gulp.watch('src/**/*.html', ['minify-html']);
  gulp.watch('src/**/*.js', ['js']);
  gulp.watch('src/images**/*', ['images']);
});
