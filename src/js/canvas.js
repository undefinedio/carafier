'use strict';

var canvas = null;

var canvas = {
  init: function (canvasId, image) {
    this.handlers();

    canvas = new fabric.Canvas(canvasId);
    //canvas.add(new fabric.Circle({radius: 30, fill: '#f55', top: 100, left: 100}));
    canvas.setBackgroundImage(image, canvas.renderAll.bind(canvas), {
      backgroundImageOpacity: 0.5,
      backgroundImageStretch: false
    });
  },

  handlers: function () {
    $('.js-add-cara').on('click', this.addCara)
  },

  addCara: function () {
    fabric.Image.fromURL('images/cara.png', function (image) {
      image.set({
        left: 0,
        top: 0,
        angle: 0
      });

      image.scale(0.3).setCoords();
      canvas.add(image);
      canvas.calcOffset();
      canvas.setActiveObject(image);
    });
  }
};

module.exports = canvas;
