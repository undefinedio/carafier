'use strict';

var mobileCheck = require('./mobilecheck.js');
var page = require('./pages.js');
var canvas = require('./canvas.js');

var width = 500;    // We will scale the photo width to this
var height = 0;     // This will be computed based on the input stream

var streaming = false;

// The various HTML elements we need to configure or control. These
// will be set by the startup() function.

var video = null;
var canvasElement = null;
var photo = null;
var startbutton = null;

var capture = {
  init: function () {
    if (mobileCheck.isMobile()) {
      this.startMobile();
    } else {
      this.startDesktop();
    }
  },

  startMobile: function(){
      //TODO add mobile functionality
  },

  startDesktop: function () {
    var _this = this;

    $('.js-allow').fadeIn();

    video = document.getElementById('video');
    canvasElement = document.getElementById('canvas');
    photo = document.getElementById('photo');
    startbutton = document.getElementById('startbutton');

    navigator.getMedia = ( navigator.getUserMedia ||
    navigator.webkitGetUserMedia ||
    navigator.mozGetUserMedia ||
    navigator.msGetUserMedia);

    navigator.getMedia(
      {
        video: true,
        audio: false
      },
      function (stream) {
        if (navigator.mozGetUserMedia) {
          video.mozSrcObject = stream;
        } else {
          var vendorURL = window.URL || window.webkitURL;
          video.src = vendorURL.createObjectURL(stream);
        }
        video.play();
      },
      function (err) {
        console.log("An error occured! " + err);
      }
    );

    this.clearphoto();

    video.addEventListener('canplay', function (ev) {
      _this.allowedCam();
    }, false);

    startbutton.addEventListener('click', function (ev) {
      _this.takepicture();
      ev.preventDefault();
    }, false);
  },

  allowedCam: function () {
    $('.js-allow').hide();
    
    page.switchPage('desktop');

    if (!streaming) {
      height = video.videoHeight / (video.videoWidth / width);

      // Firefox currently has a bug where the height can't be read from
      // the video, so we will make assumptions if this happens.
      if (isNaN(height)) {
        height = width / (4 / 3);
      }

      video.setAttribute('width', width);
      video.setAttribute('height', height);
      canvasElement.setAttribute('width', width);
      canvasElement.setAttribute('height', height);
      streaming = true;
    }
  },


  clearphoto: function () {
    var context = canvasElement.getContext('2d');
    context.fillStyle = "#AAA";
    context.fillRect(0, 0, canvasElement.width, canvasElement.height);
  },

  takepicture: function () {
    page.switchPage('cara');

    var context = canvasElement.getContext('2d');

    if (width && height) {
      canvasElement.width = width;
      canvasElement.height = height;
      context.drawImage(video, 0, 0, width, height);
      canvas.init("canvas", data);
    } else {
      this.clearphoto();
    }
  }
};


module.exports = capture;
