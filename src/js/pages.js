'use strict';

var pages = {
  switchPage: function (page) {
    $('.page').removeClass('open');
    $('.page.'+page).addClass('open');
  }
};

module.exports = pages;
